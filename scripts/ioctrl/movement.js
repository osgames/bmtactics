function MoveRandomly(x, y, w, h) { // implement x y w h etc later
 var commands = new Array(COMMAND_MOVE_NORTH, COMMAND_MOVE_SOUTH, COMMAND_MOVE_EAST, COMMAND_MOVE_WEST, COMMAND_WAIT);
 var r = Math.floor(Math.random() * commands.length);
 Move(GetCurrentPerson(), commands[r], 1, false);
 }

function CanMoveInDirection(name, direction, tiles) {
 switch (direction) {
  case ("north"): return CanMove(name, COMMAND_MOVE_NORTH, tiles); break;
  case ("south"): return CanMove(name, COMMAND_MOVE_SOUTH, tiles); break;
  case ("east"):  return CanMove(name, COMMAND_MOVE_EAST,  tiles); break;
  case ("west"):  return CanMove(name, COMMAND_MOVE_WEST,  tiles); break;
  }
 return false;
 }

function CanMove(name, command, tiles) {
 switch (command) {
  case COMMAND_MOVE_NORTH:
   for (var i = 0; i < GetTileHeight() * tiles; ++i)
    if(IsPersonObstructed(name, GetPersonX(name), GetPersonY(name) - i))
     return false;
    return true;
   break;
  case COMMAND_MOVE_SOUTH:
   for (var i = 0; i < GetTileHeight() * tiles; ++i)
    if(IsPersonObstructed(name, GetPersonX(name), GetPersonY(name) + i))
     return false;
    return true;
   break;
  case COMMAND_MOVE_EAST:
   for (var i = 0; i < GetTileWidth() * tiles; ++i)
    if(IsPersonObstructed(name, GetPersonX(name) + i, GetPersonY(name)))
     return false;
    return true;
   break;
  case COMMAND_MOVE_WEST:
   for (var i = 0; i < GetTileWidth() * tiles; ++i)
    if(IsPersonObstructed(name, GetPersonX(name) - i, GetPersonY(name)))
     return false;
    return true;
   break;
  case COMMAND_WAIT:
   return true; // duh ^_^
   break;
  }
 return false;
 }

/**
  Tells person 'name' to move a number of tiles
  e.g. Move("Jimmy", COMMAND_MOVE_NORTH, 1, false);
*/
function Move(name, command, tiles, immediate) {
 switch (command) {
  case COMMAND_MOVE_NORTH:
   QueuePersonCommand(name, COMMAND_FACE_NORTH, immediate);
   for (var i = 0; i < GetTileHeight() * tiles; ++i)
    QueuePersonCommand(name, command, false);
   break;
  case COMMAND_MOVE_SOUTH:
   QueuePersonCommand(name, COMMAND_FACE_SOUTH, immediate);
   for (var i = 0; i < GetTileHeight() * tiles; ++i)
    QueuePersonCommand(name, command, false);
   break;
  case COMMAND_MOVE_EAST:
   QueuePersonCommand(name, COMMAND_FACE_EAST, immediate);
   for (var i = 0; i < GetTileWidth() * tiles; ++i)
    QueuePersonCommand(name, command, false);
   break;
  case COMMAND_MOVE_WEST:
   QueuePersonCommand(name, COMMAND_FACE_WEST, immediate);
   for (var i = 0; i < GetTileWidth() * tiles; ++i)
    QueuePersonCommand(name, command, false);
   break;
  case COMMAND_WAIT:
   for (var i = 0; i < GetTileWidth() * tiles; ++i)
    QueuePersonCommand(name, command, false);
   break;
  }
 }

/**
  Tells person_name to face the input person
*/
function FacePlayer(p_name,p_to_face, threshX,threshY) { 
 var horiz = ["east","west"];
 var vert = ["north","south"];
 var dirStr = "";
 threshX = (threshX)?parseInt(threshX):GetTileWidth();
 threshY = (threshY)?parseInt(threshY):GetTileHeight();
 if (IsInputAttached()) {
  if (!p_to_face||!findPerson(p_to_face)) p_to_face = GetInputPerson();
  var p_x = GetPersonX(p_to_face);
  var p_y = GetPersonY(p_to_face);
  var c_x = GetPersonX(p_name);
  var c_y = GetPersonY(p_name);
  if (p_y<c_y-threshY) dirStr = vert[0];
  else if (p_y>c_y+threshY) dirStr = vert[1];
  if (p_x>c_x+threshX) dirStr += horiz[0];
  else if (p_x<c_x-threshX) dirStr += horiz[1];
  if (dirStr=="") { // if vague location doesn't work cut leeway in half
   if (p_y<c_y-threshY/2) dirStr = vert[0];
   else if (p_y>c_y+threshY/2) dirStr = vert[1];
   if (p_x>c_x+threshX/2) dirStr += horiz[0];
   else if (p_x<c_x-threshX/2) dirStr += horiz[1];
   }
  if (dirStr=="") { // if specific location doesn't work don't approx
   if (p_y<c_y) dirStr = vert[0];
   else if (p_y>c_y) dirStr = vert[1];
   if (p_x>c_x) dirStr += horiz[0];
   else if (p_x<c_x) dirStr += horiz[1];
   }
  //QueuePersonCommand(person_name,eval("COMMAND_FACE_"+toUpper(dirStr)),true);
  if (GetPersonDirection(p_name)!=dirStr) {
   SetPersonDirection(p_name,dirStr);
   SetPersonFrame(p_name,0);
   if (IsMapEngineRunning()) RenderMap();
   }
  }
 return dirStr; 
 }

/**
  Intended to be used in OnGenerateCommands
  i.e. FollowPath(GetCurrentPerson(),  "EEENNNWWWWSSSE");
  i.e. SetPersonScript("Jimmy", SCRIPT_COMMAND_GENERATOR, 'FollowPath("Jimmy",  "NNDESSWF")');
  Where the path is a string of N, E, S, W, D, F (north, east, south, west, delay, finish)
  It returns:
    -1 = the person's path has finished (encountered an F)
     0 = the person has not moved
     1 = the person has moved
*/
function FollowPath(person_name, path) {
 var path_state = 0;
 if ("list" in FollowPath == false) FollowPath.list = new Object();
 function GetFollowPathDelta(path, index) {
  var delta = 0;
  if (index >= 0 && index < path.length) {
   switch (path.charAt(index)) {
    case "N": delta = GetTileHeight(); break;
    case "S": delta = GetTileHeight(); break;
    case "E": delta = GetTileWidth(); break;
    case "W": delta = GetTileWidth(); break;
    case "D": delta = Math.sqrt(GetTileWidth() * GetTileHeight()); break;
    case "F": delta = Math.sqrt(GetTileWidth() * GetTileHeight()); break;
    } 
   }
  return delta;
  }
 if (person_name in FollowPath.list == false) {
  FollowPath.list[person_name] = new Object();
  FollowPath.list[person_name].name = person_name;
  FollowPath.list[person_name].path = path;
  FollowPath.list[person_name].index = 0;
  FollowPath.list[person_name].delta = GetFollowPathDelta(path, 0);
  }
 if (FollowPath.list[person_name].path != path) {
  FollowPath.list[person_name].path = path;
  FollowPath.list[person_name].index = 0;
  FollowPath.list[person_name].delta = GetFollowPathDelta(path, 0);
  }
 if (FollowPath.list[person_name].delta == 0) {
  if (FollowPath.list[person_name].path.charAt(FollowPath.list[person_name].index) != "F")
   FollowPath.list[person_name].index++;
  FollowPath.list[person_name].delta = GetFollowPathDelta(path, FollowPath.list[person_name].index);
  }
 if (FollowPath.list[person_name].index >= path.length) {
  FollowPath.list[person_name].index = 0;
  FollowPath.list[person_name].delta = GetFollowPathDelta(path, FollowPath.list[person_name].index);
  }
 if (FollowPath.list[person_name].index >= 0 && FollowPath.list[person_name].index < path.length) {
  var dx = GetPersonX(person_name);
  var dy = GetPersonY(person_name);
  switch (FollowPath.list[person_name].path.charAt(FollowPath.list[person_name].index)) {
   case "N": dy -= 1; QueuePersonCommand(person_name, COMMAND_FACE_NORTH, true);
    if (!IsPersonObstructed(person_name, dx, dy)) {
     QueuePersonCommand(person_name, COMMAND_MOVE_NORTH, false);
     FollowPath.list[person_name].delta--; path_state = 1;
     } break;
   case "S": dy += 1; QueuePersonCommand(person_name, COMMAND_FACE_SOUTH, true);
    if (!IsPersonObstructed(person_name, dx, dy)) {
     QueuePersonCommand(person_name, COMMAND_MOVE_SOUTH, false);
     FollowPath.list[person_name].delta--; path_state = 1;
     } break;
   case "E": dx += 1; QueuePersonCommand(person_name, COMMAND_FACE_EAST,  true);
    if (!IsPersonObstructed(person_name, dx, dy)) {
     QueuePersonCommand(person_name, COMMAND_MOVE_EAST, false);
     FollowPath.list[person_name].delta--; path_state = 1;
     } break;
   case "W": dx -= 1; QueuePersonCommand(person_name, COMMAND_FACE_WEST,  true);
    if (!IsPersonObstructed(person_name, dx, dy)) {
     QueuePersonCommand(person_name, COMMAND_MOVE_WEST, false);
     FollowPath.list[person_name].delta--; path_state = 1;
     } break;
   case "D": QueuePersonCommand(person_name, COMMAND_WAIT, false);
    FollowPath.list[person_name].delta--; break;
   case "F": path_state = -1; break;
   }
  }
 return path_state;
 }

