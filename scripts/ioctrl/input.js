/* BMPROJECT FILE: ioctrl/input.js - Custom User Input Routines

SYNOPSIS:
	This file controls how users input information into the program.

EXECUTION:
	None.

CONTENT:
	Contains the zeroinit.js header information, includes, global variables
	Contains the GetKeyStringEX() keyboard to text driver, which is (at
	least in my opinion) much better than the input.js system script. Also
	contains some example functions and library functions for reuse.

NOTES:
	Deprecates the input.js system script.

LICENSE(S):
	GNU General Public License (see zeroinit.js)

*/
// Betrayer's Moon user input Tuesday, November 8, 2005

// KeyExceptions defines which keys should not converted to strings by GetKeyStringEX()
const KeyExceptions = new Array(KEY_ENTER,KEY_ESCAPE,KEY_BACKSPACE);

const AllKeys = new Array(KEY_ESCAPE,
KEY_F1,KEY_F2,KEY_F3,KEY_F4,KEY_F5,KEY_F6,KEY_F7,KEY_F8,KEY_F9,KEY_F10,KEY_F11,KEY_F12,
KEY_TILDE,KEY_0,KEY_1,KEY_2,KEY_3,KEY_4,KEY_5,KEY_6,KEY_7,KEY_8,KEY_9,
KEY_MINUS,KEY_EQUALS,KEY_BACKSPACE,KEY_TAB,
KEY_A,KEY_B,KEY_C,KEY_D,KEY_E,KEY_F,KEY_G,KEY_H,KEY_I,KEY_J,KEY_K,KEY_L,KEY_M,
KEY_N,KEY_O,KEY_P,KEY_Q,KEY_R,KEY_S,KEY_T,KEY_U,KEY_V,KEY_W,KEY_X,KEY_Y,KEY_Z,
KEY_SHIFT,KEY_CTRL,KEY_ALT,KEY_SPACE,KEY_OPENBRACE,KEY_CLOSEBRACE,KEY_SEMICOLON,KEY_APOSTROPHE,
KEY_COMMA,KEY_PERIOD,KEY_SLASH,KEY_BACKSLASH,KEY_ENTER,KEY_INSERT,KEY_DELETE,KEY_HOME,
KEY_END,KEY_PAGEUP,KEY_PAGEDOWN,KEY_UP,KEY_RIGHT,KEY_DOWN,KEY_LEFT);

const AllKeysString = new Array("KEY_ESCAPE",
"KEY_F1","KEY_F2","KEY_F3","KEY_F4","KEY_F5","KEY_F6","KEY_F7","KEY_F8","KEY_F9","KEY_F10","KEY_F11","KEY_F12",
"KEY_TILDE","KEY_0","KEY_1","KEY_2","KEY_3","KEY_4","KEY_5","KEY_6","KEY_7","KEY_8","KEY_9",
"KEY_MINUS","KEY_EQUALS","KEY_BACKSPACE","KEY_TAB",
"KEY_A","KEY_B","KEY_C","KEY_D","KEY_E","KEY_F","KEY_G","KEY_H","KEY_I","KEY_J","KEY_K","KEY_L","KEY_M",
"KEY_N","KEY_O","KEY_P","KEY_Q","KEY_R","KEY_S","KEY_T","KEY_U","KEY_V","KEY_W","KEY_X","KEY_Y","KEY_Z",
"KEY_SHIFT","KEY_CTRL","KEY_ALT","KEY_SPACE","KEY_OPENBRACE","KEY_CLOSEBRACE","KEY_SEMICOLON","KEY_APOSTROPHE",
"KEY_COMMA","KEY_PERIOD","KEY_SLASH","KEY_BACKSLASH","KEY_ENTER","KEY_INSERT","KEY_DELETE","KEY_HOME",
"KEY_END","KEY_PAGEUP","KEY_PAGEDOWN","KEY_UP","KEY_RIGHT","KEY_DOWN","KEY_LEFT");

function ConvertKeyToString(key) {
	for(var i = 0; i < AllKeys.length; i++) {
		eval("if(AllKeys["+i+"] == "+AllKeysString[i]+") return "+AllKeysString[i]+";");
	}
	return false;
}

function WaitForKeyPress() {
	var ss = GetScreenBuffer();
	while(!(AreKeysLeft() && IsAnyKeyPressed())) {
		ss.blit(0,0);
		FlipScreen();
	}
}

function ClearKeyQueue() {
	while(AreKeysLeft()) {void GetKey();}
}

function GetInputPersonX() {
	return GetPersonX(GetInputPerson());
}

function GetInputPersonY() {
	return GetPersonY(GetInputPerson());
}

try {notafunction();} catch(v) {TestRecord("GetKeyStringEX",new Array(),v.lineNumber+2,v.fileName,false);}
function GetKeyStringEX() {
	var key = GetKey();
	for(var i = 0; i < KeyExceptions.length; i++) {
		if(key == KeyExceptions[i]) {return key;}
	}
	return GetKeyString(key,IsKeyPressed(KEY_SHIFT));
}

try {notafunction();} catch(v) {TestRecord("SimpleInputPrompt",new Array("prompttext"),v.lineNumber+2,v.fileName,false);}
function SimpleInputPrompt(prompttext) {
	var currenttext = "";
	var donewithinput = false;
	var buffer;
	while(!donewithinput) {
		TextFont.drawText(0,10,prompttext);
		TextFont.drawText(0,30,currenttext);
		FlipScreen();
		buffer = GetKeyStringEX();//GetKeyString(GetKey(),IsKeyPressed(KEY_SHIFT));//TranslateKeypress(/*GetKey()*/);
		switch(buffer) {
			case KEY_ENTER:
				donewithinput = true;
				break;
			case KEY_BACKSPACE:
				if(currenttext.length > 0) currenttext = currenttext.substring(0,currenttext.length-1);
				break;
			default:
				currenttext = currenttext + buffer;
				break;
		}
	}
	return currenttext;
}

/* The Input_BoxLibrary() function

	This function can, but is not meant to, be called by itself. Instead, it works by using a wrapper
	as such:
	
	function InputArgument(argument) {return Input_BoxLibrary("INPUT ARGUMENT: ",argument);}
	
	Stupid, I know. But it's for legacy support because I don't feel like rewriting DevMenu.

*/
function Input_BoxLibrary(text,argument) { // modified version of SimpleInputPrompt from input.js
	var currenttext = "";
	var donewithinput = false;
	var buffer;
	ClearScreenBuffer();
	while(!donewithinput) {
		InputBox(StartWindowStyle,currenttext,text.toUpperCase() + argument.toUpperCase());
		FlipScreen();
		ClearKeyQueue();
		buffer = GetKeyStringEX();
		switch(buffer) {
			case KEY_ENTER:
				donewithinput = true;
				break;
			case KEY_ESCAPE:
				return KEY_ESCAPE;
				break;
			case KEY_BACKSPACE:
				if(currenttext.length > 0) currenttext = currenttext.substring(0,currenttext.length-1);
				break;
			default:
				currenttext = currenttext + buffer;
				break;
		}
	}
	return currenttext;
}

var skb_person,skb_direction,skb_obstruct,skb_base_width,skb_base_height,skb_iia,skb_magic,skb_x,skb_y;

// This is used in the update script
function ScanKeyBindings() { // I don't think I'll need keybind.js anymore...
	skb_iia = IsInputAttached();
	/* This next line uses the internal engine's AttachInput method of character input.
	 * However, we use our own driver to run the directions and block other keys from
	 * being caught by the system.
	 */
	skb_person = skb_iia? GetInputPerson() : false;
	SetTalkActivationKey(userprefs["actionkey"].current);
	if(AreKeysLeft()) {
		skb_magic = GetKey();
	} else {
		skb_magic = NaN; // NaN matches nothing, including itself. So that's what we want to use.
	}
	if(IsKeyPressed(userprefs["downkey"].current) && IsKeyPressed(userprefs["leftkey"].current)) {
		if(skb_iia) {
			QueuePersonCommand(skb_person,COMMAND_FACE_SOUTHWEST,true);
			QueuePersonCommand(skb_person,COMMAND_MOVE_SOUTH,true);
			QueuePersonCommand(skb_person,COMMAND_MOVE_WEST,true);
		}
	} else if(IsKeyPressed(userprefs["upkey"].current) && IsKeyPressed(userprefs["leftkey"].current)) {
		if(skb_iia) {
			QueuePersonCommand(skb_person,COMMAND_FACE_NORTHWEST,true);
			QueuePersonCommand(skb_person,COMMAND_MOVE_NORTH,true);
			QueuePersonCommand(skb_person,COMMAND_MOVE_WEST,true);
		}
	} else if(IsKeyPressed(userprefs["downkey"].current) && IsKeyPressed(userprefs["rightkey"].current)) {
		if(skb_iia) {
			QueuePersonCommand(skb_person,COMMAND_FACE_SOUTHEAST,true);
			QueuePersonCommand(skb_person,COMMAND_MOVE_SOUTH,true);
			QueuePersonCommand(skb_person,COMMAND_MOVE_EAST,true);
		}
	} else if(IsKeyPressed(userprefs["upkey"].current) && IsKeyPressed(userprefs["rightkey"].current)) {
		if(skb_iia) {
			QueuePersonCommand(skb_person,COMMAND_FACE_NORTHEAST,true);
			QueuePersonCommand(skb_person,COMMAND_MOVE_NORTH,true);
			QueuePersonCommand(skb_person,COMMAND_MOVE_EAST,true);
		}
	} else if(IsKeyPressed(userprefs["downkey"].current)) {
		if(skb_iia) {
			QueuePersonCommand(skb_person,COMMAND_FACE_SOUTH,true);
			QueuePersonCommand(skb_person,COMMAND_MOVE_SOUTH,true);
		}
	} else if(IsKeyPressed(userprefs["upkey"].current)) {
		if(skb_iia) {
			QueuePersonCommand(skb_person,COMMAND_FACE_NORTH,true);
			QueuePersonCommand(skb_person,COMMAND_MOVE_NORTH,true);
		}
	} else if(IsKeyPressed(userprefs["leftkey"].current)) {
		if(skb_iia) {
			QueuePersonCommand(skb_person,COMMAND_FACE_WEST,true);
			QueuePersonCommand(skb_person,COMMAND_MOVE_WEST,true);
		}
	} else if(IsKeyPressed(userprefs["rightkey"].current)) {
		if(skb_iia) {
			QueuePersonCommand(skb_person,COMMAND_FACE_EAST,true);
			QueuePersonCommand(skb_person,COMMAND_MOVE_EAST,true);
		}
	} else if(IsKeyPressed(userprefs["actionkey"].current)) {
		void false; // allow the actionkey to pass to the engine
	} else if(skb_magic == userprefs["cancelkey"].current) { // Any one-press key event should be defined here, like this
		if(skb_iia) {
			skb_x = GetPersonScreenX(GetInputPerson())+GetTileWidth()/2+1;
			skb_y = GetPersonScreenY(GetInputPerson());
		} else {
			skb_x = 1;
			skb_y = 1+CaptFontHeight;
		}
		escmenu.go(skb_x,skb_y); // That, my friend, was an exceptionally slick fix.
	} else if(IsAnyKeyPressed()) { // block the keys we don't want to use by default... i.e. the ones not in this list
		if(skb_iia) QueuePersonCommand(skb_person,COMMAND_WAIT,true);
	}
	ClearKeyQueue();
}

try {notafunction();} catch(k) {TotalLineCount += k.lineNumber+1; FileLineCount[k.fileName.replace(/scripts\//i,"")] = k.lineNumber+1;}