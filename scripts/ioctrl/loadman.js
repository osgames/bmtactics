// Now loading screen, by dremelofdeath for Betrayer's Moon
function LoadManagerService() {
	this.isCurrentlyLoading = false;
	this.NowLoadingImage = LoadImage("loadman.png");
	this.imagex = GetScreenWidth() - this.NowLoadingImage.width;
	this.imagey = GetScreenHeight() - this.NowLoadingImage.height;
}
LoadManagerService.prototype.NLMapChange = function(map,x,y,changingmusic,music,needsupdation) {
	if(needsupdation == undefined) needsupdation = false;
	var speed = .25
	var meow = CreateSurface(GetScreenWidth(),GetScreenHeight(),CreateColor(0,0,0));
	meow.setAlpha(0);
	if(!changingmusic || changingmusic == undefined) music == undefined;
	var ss = GetScreenBuffer();
	for(var i = 1; i <= 255; i += 3) {
		if(needsupdation && IsMapEngineRunning()) {
			UpdateMapEngine();
			RenderMap();
		} else {
			ss.blit(0,0);
		}
		if(i > 255) {
			meow.setAlpha(255);
		} else {
			meow.setAlpha(i);
		}
		meow.blit(0,0);
		FlipScreen();
	}
	meow.setAlpha(255);
	meow.blit(0,0);
	this.NowLoadingImage.blit(this.imagex,this.imagey);
	FlipScreen();
	if(changingmusic && !Global.disableAllMusic) {
		eval("var asdf = MusicLibrary."+music+";");
		asdf.LoadMusic();
	}
	var person = GetInputPerson();
	ChangeMap(map);
	SetPersonX(person, x);
	SetPersonY(person, y);
	if(changingmusic && !Global.disableAllMusic) {
		Jukebox.CrossfadeBGM(asdf.sound,true,speed);
		wait(225);
	}
	var sss = GetScreenBuffer().createSurface();
	UpdateMapEngine();
	RenderMap();
	ss = GetScreenBuffer();
	for(var i = 254; i >= 0; i -= 3) {
		if(needsupdation && IsMapEngineRunning()) {
			UpdateMapEngine();
			RenderMap();
		} else {
			ss.blit(0,0);
		}
		if(i < 0) {
			sss.setAlpha(0);
		} else {
			sss.setAlpha(i);
		}
		sss.blit(0,0);
		FlipScreen();
	}
}
var LoadManager = new LoadManagerService();
try {notafunction();} catch(k) {TotalLineCount += k.lineNumber+1; FileLineCount[k.fileName.replace(/scripts\//i,"")] = k.lineNumber+1;}