// Dialogue engine for Betrayer's Moon by dremelofdeath
function PortraitService() {
	this.Images = new Object();
	// Preload all of the images here-- this.Images.Character = new Object(); this.Images.Character.expression = LoadImage("Character_expression.png");
	// Ripley
	this.Images.Ripley = new Object();
	this.Images.Ripley.normal = LoadImage("Ripley_normal.png");
	// Rothengen
	this.Images.Rothengen = new Object();
	this.Images.Rothengen.normal = LoadImage("Rothengen_normal.png");
}
PortraitService.prototype.JustBlitPerson = function(person,expression,spot) {
	eval("var mew = this.Images."+person+"."+expression+";");
	switch(spot.toLowerCase()) {
		case "right":
			mew.blit(GetScreenWidth()-mew.width,GetScreenHeight()-mew.height);
			break;
		case "left":
			mew.transformBlit(mew.width,GetScreenHeight()-mew.height,0,GetScreenHeight()-mew.height,0,GetScreenHeight(),mew.width,GetScreenHeight());
			break;
	}
}
var Portrait = new PortraitService();

/* The Chapter object
The Chapter object keeps track of chapters in the index. It has children objects, called
Scenes. Chapter objects, in themselves, contain no dialogue. Only the children Scenes
contain the dialogue. You might see a Chapter object declared like so (always use the
MakeChapter() function):

MakeChapter(4,"A New Hope");

Or alternatively you could use MakeNextChapter(), which automatically takes care of the
numbering for you:

MakeNextChapter("The Phantom Menace"); //chapter 1
MakeNextChapter("The Clone Wars"); //chapter 2
MakeNextChapter("The Revenge of the Sith"); //chapter 3

There can be only one entry per chapter. Chapters may only be declared once, and after
they are declared they cannot be erased from memory. Only integers may be entered for
the number entry. If a floating point number is entered (for example 1.6), Math.floor()
slices said number into an integer. An error is returned and the program panics if a
Chapter entry tries to overwrite another Chapter.

As a side note, you should try to be consistent on which approach you take between
MakeChapter() and MakeNextChapter(). Switching could confuse the compiler. And also,
if you are using MakeChapter(), you must define every chapter you will use. You can
have Chapters 1-9, for example, but only if all of them are defined. You can't have
something to the effect of Chapters 1-9 but not 3 or 5. That will kill the compiler.
*/

var chapterindex = new Array(); // added to remove global.js
var currentchapter = 0; // These were added for the removal of global.js... the slime it was
var currentscene = 0; // global.js *hurl die*

function Chapter(number,title) {
	this.title = title;
	this.number = Math.floor(number);
	this.inited = true;
	this.sceneIndex = new Array();
}
function MakeChapter(number,title) {
	if(Global.chapterIndex[Math.floor(number)-1].inited) {
		Abort("Duplicate Chapters may not exist.");
		return false;
	}
	Global.chapterIndex[Math.floor(number)-1] = new Chapter(number,title);
}
function MakeNextChapter(title) {
	var number = chapterindex.length;
	chapterindex.push(new Chapter(number+1,title));
}
function GetChapter(number) {
	return chapterindex[Math.floor(number)-1];
}
function GetChapterTitle(number) {
	return GetChapter(number).title;
}
/* The Scene object
The Scene object contains information pertaining to the actual dialogue of a specific
Scene in a specific Chapter. At startup, the dialogue is compiled and ready to go (this
may change in a later version). As with Chapters, duplicate Scenes may not exist.
*/
function Scene(number,title) {
	this.title = title;
	this.number = Math.floor(number);
	this.inited = true;
	this.dialogue = new Array();
	this.go = undefined; // do not set go... compile will do that
	// these are the headers for the go() function... executed first in the scene
	this.headers = ""; // not done yet
}
function MakeScene(chapter,number,title) {
	if(GetChapter(chapter).sceneIndex[Math.floor(number)-1].inited) {
		Abort("Duplicate Scenes may not exist.");
		return false;
	}
	GetChapter(chapter).sceneIndex[Math.floor(number)-1] = new Scene(number,title);
}
function MakeNextScene(chapter,title) {
	var number = GetChapter(chapter).sceneIndex.length;
	GetChapter(chapter).sceneIndex.push(new Scene(number+1,title));
	// Haha, I'm doing pretty well considering I wrote this code at 4:50 AM.
	// Thank God for Mountain Dew Code Red!
}
function GetScene(chapter,number) {
	return GetChapter(chapter).sceneIndex[Math.floor(number)-1];
}
function GetSceneTitle(chapter,number) {
	return GetScene(chapter,number).title;
	// Preliminary testing says that these functions are GOOD.
	// And all this at almost 5:00 AM!!
}
// This GetScene function is a child of the Chapter function... you need not
// specify a chapter when using this one
Chapter.prototype.GetScene = function(number) {
	return this.sceneIndex[Math.floor(number)-1];
}
// The pw() function starts a new line of Playwright code.
// Compile variables can be found in global.js
function pw() {
	return GetChapter(currentchapter).GetScene(currentscene);
}
// GoScene() is the quicker way to load a scene and play it
function GoScene(chapter,scene) {
	GetChapter(chapter).GetScene(scene).go();
}
/* The compile method of the Scene object
The compile() function will compile any handwritten dialogue into a format readable by
the internal ConversationEngine. This will allow non-programmers who need to write dialogue
for BM to put the dialogue directly into the code and autocompile it on startup. This
might slow the startup down a minute amount, but it will be worth it. Dialogue will be
organized into the respective dialogue folder by scene and chapter. include.js will load
the toc.js, which will recursively compile the dialogue. This might be rewritten later to
compile on a need by need basis. 

sourcearray is the source dialogue, with each line a new array entry. For example,
sourcearray[0] is the first line of dialogue. The second line is sourcearray[1], and so
on.

The format for lines is such: each line begins with the person speaking it, followed by a
colon. This will give a line such as this:

sourcearray.push() = "Ripley (normal): Hey, wait a second! That's no cheeseburger!"

That would be compiled to a window with Ripley talking and his normal expression portrait
displayed, with window title RIPLEY. The text then scrolls, letter by letter. Dialogue
should be compiled into a function, and called by Chapter and Scene index, followed by a
go() method:

GetChapter(3).GetScene(2).go();

That's the dialogue scene of Chapter 3, Scene 2. A simpler and more effective way to do
this would be as so:

GoScene(3,2);

*/

// By the way, before I forget, I hereby copyright and entitle my new psuedo language...'
// "Playwright™"
// ...or something...
Scene.prototype.compile = function() {
	var functionbuffer = "";
	var speaker = "";
	var expression = "";
	var command = "";
	var ctd = "";
	for(var i = 0; i < this.dialogue.length; i++) {
		// Let's read what we want to compile first
		ctd = this.dialogue[i];
		// Probe for known commands
		if(ctd.match(/\((enter left|enter right|enter|exit|all exit)/i)) {
			// If a command is present, compile as a command
			// Let's get the command used
			command = ctd.match(/\((enter left|enter right|enter|exit|all exit)/i)[1];
			// Remove the command chunk
			ctd = ctd.replace(/\((enter left|enter right|enter|exit|all exit)\s/i,"");
			// Now find the subject
			speaker = ctd.match(/(\S+)\S/)[1];
			// Remove the subject
			ctd = ctd.replace(/(\S+)\S/,"");
			// Find an expression
			ctd = ctd.replace(/\[/,"");
			ctd = ctd.replace(/\]/,"");
			expression = ctd.match(/(\S+)\S/)[1];
			// Now just merge the code into JS
			functionbuffer = functionbuffer // insert code here!! lkasjdfkljasdhf laksjfhasdk
			// Note to self: add conditional branches for different commands
			// All done!
		} else {
			// If there is no command present, compile it as dialogue
			// Get the speaker
			speaker = ctd.match(/^(\S+)\s/)[1];
			// Now, remove the speaker from the line
			ctd = ctd.replace(/^(\S+)\s/,"");
			// We want the expression for the portrait
			expression = ctd.match(/(\w+)\w/)[1];
			// Again, remove the expression
			ctd = ctd.replace(/(\w+)\w/,"");
			// Now we should have some garbage to take care of: "():"
			ctd = ctd.replace(/\(\):\s/,"");
			// The remaining text should be pure dialog.
			// That being the case, let's merge them into JS!
			functionbuffer = functionbuffer //insert code here!! aksjdfhadklfjahdkfh asdjdfhla
		}
	}
	// Since that's done, let's flush the buffer into a real live function!
	eval("this.go = function() {\n" + this.headers + "\n" + functionbuffer + "\n}");
	// This one's done! I finished it at 6:51 AM! Not too bad for an all-nighter...
}
try {notafunction();} catch(k) {TotalLineCount += k.lineNumber+1; FileLineCount[k.fileName.replace(/scripts\//i,"")] = k.lineNumber+1;}