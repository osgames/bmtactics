/* BMPROJECT FILE: videoui/update.js - Update Script for Map Engine

SYNOPSIS:
	This is the update script for the Sphere map engine.

EXECUTION:
	This function is called right before the map engine renders a
	frame.

CONTENT:
	The update script and supporting functions.

LICENSE(S):
	GNU General Public License (see zeroinit.js)

*/

function UpdateScript() {
	ScanKeyBindings();
}

SetUpdateScript("try {UpdateScript();} catch(error) {HandleException(error); Exit();}");