/* BMPROJECT FILE: videoui/render.js - Render Script for Map Engine

SYNOPSIS:
	This is the render script for the Sphere map engine.

EXECUTION:
	This function is called every time the map engine finishes rendering a
	frame.

CONTENT:
	The render script and supporting functions.

LICENSE(S):
	GNU General Public License (see zeroinit.js)

*/



function RenderScript() {}

SetRenderScript("try {RenderScript();} catch(error) {HandleException(error); Exit();}");