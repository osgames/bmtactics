/* BMPROJECT FILE: ioctrl/window.js - Window and Textbox Functions

SYNOPSIS:
	This file defines the systems that render windows to the screen buffer.

EXECUTION:
	Defines a few universal variables intended for reuse.

CONTENT:
	Contains all of the functions that should be used to display windows
	and text in BM. Also contains some sparse variables for consistency.

NOTES:
	Many, many other scripts in BM hinge on this one script.

LICENSE(S):
	GNU General Public License (see zeroinit.js)

*/
//BM's WindowStyle Functions! dremelofdeath

const CaptFontHeight = CaptFont.getHeight();
const TextFontHeight = TextFont.getHeight();

const BoxNumberOfLines = 8; // I don't this used anymore

// This is used (or will be used) for scrolling TalkBox() and is currently linked to the Menu()'s MenuDownArrow
const WindowMoreImage = LoadImage("ui/curdown.png");

const _TestText = "This is a lot of text used mainly to test TalkBox().~p2 It can also be used to test UniversalBox() if necessary,\nbut I doubt it would have to come to that. Homeboys come and homeboys go, but I still kick the latin lingo...";
const _TestText2 = "adfdfasfasfdassadadfdfasfasf~p2dassadadfdfasfa~p1sfdassadadfdfa~p3~p5sfasfdassadadfdfasfasfdassadadfdfasfasfdassadadfdfasfasfdassadadfdfasfasfdassadadfdfasfasfdassadadfdfasfasfdassadadfdfasfasfdassadadfdfasfasfdassadadfdfasfasfdassad";

// BEHOLD, THE NEW STRING WRAPPER
//                       ^^ I think it works!!

try {notafunction();} catch(v) {TestRecord("StringWrap",new Array("width","texttowrap"),v.lineNumber+2,v.fileName,false);}
function StringWrap(width,texttowrap) {
	var broken = texttowrap.split(" ");
	var sigma = new Array();
	var xi = new Array();
	var delta = TextFont.getStringWidth("X");
	var word,upsilon;
	var current = broken.shift();
	var modifier = 0; // to compensate for any possible commands
	var cmdlength = TextFont.getStringWidth("~X0");
	
	while(broken.length > 0) {
		word = broken.shift();
		if(/\n\n/.test(word)) { // StringWrap does NOT support more than 2 line breaks at a time. Not like you'll ever need more than that.
			xi = word.split(/\n\n/);
			while(xi.length-1 > 0) {
				upsilon = xi.shift();
				if(TextFont.getStringWidth(current)+delta+TextFont.getStringWidth(upsilon)-modifier<=width) {
					current += " "+upsilon;
				} else {
					sigma.push(current);
					current = upsilon;
					modifier = 0;
				}
				sigma.push(current);
				sigma.push("");
				modifier = 0;
				current = xi.shift();
			}
		} else if(/\n/.test(word)) {
			xi = word.split(/\n/);
			while(xi.length-1 > 0) {
				upsilon = xi.shift();
				if(TextFont.getStringWidth(current)+delta+TextFont.getStringWidth(upsilon)-modifier<=width) {
					current += " "+upsilon;
				} else {
					sigma.push(current);
					current = upsilon;
					modifier = 0;
				}
				sigma.push(current);
				modifier = 0;
				current = xi.shift();
			}
		} else if(/~\w[\d|\w]/i.test(word)) { // checks for commands
			if(TextFont.getStringWidth(current)+delta+TextFont.getStringWidth(word)-modifier-cmdlength<=width) {
				current += " "+word;
				modifier += cmdlength;
			} else {
				sigma.push(current);
				current = word;
				modifier = cmdlength;
			}
		} else {
			if(TextFont.getStringWidth(current)+delta+TextFont.getStringWidth(word)-modifier<=width) {
				current += " "+word;
			} else {
				sigma.push(current);
				current = word;
				modifier = 0;
			}
		}
	}
	sigma.push(current);
	return sigma; // Eat your heart out, Gospel.
}

function LineBreakString(width,texttowrap) {
	var sigma = StringWrap(width,texttowrap);
	var current = "";
	for(var i = 0; i < sigma.length; i++) {current += i==sigma.length-1? sigma[i] : sigma[i]+"\n";}
	return current;
}

function ExplodeLineBreakString(texttoexplode) {
	return texttoexplode.split("\n");
}

function ReformatSigmaELB(taintedsigma) {
	var sigma = new Array();
	for(var i = 0; i < taintedsigma.length; i++) {
		for(var j = 0; j < taintedsigma[i].split("\n").length; j++) {
			sigma.push(taintedsigma[i].split("\n")[j]);
		}
	}
	return sigma;
}

// This is some stuff that was merged from zeroinit.js. It just makes sense to have
// it here, considering it has no business in the headers as it only pertains to the
// windowstuffs...

var WindowTitles = new Array(); // Standardized window titles
WindowTitles["system"] = "SYSTEM MESSAGE";
WindowTitles["mainmenu"] = "MAIN MENU";
WindowTitles["options"] = "OPTIONS";

// bmgame font rests, thanks to dremelofdeath
// By the way, a full character is 8 pixels (X)
const WholeRest = "更更更更";
const HalfRest = "十十";
const QuarterRest = "室";
const EighthRest = "�";
const DotDotDot = "�.�.�.更更更更"/**/ // keep these right now until I can remove them
// See the new TalkBox function for more information... there is a new switch block there
const text_full_pause_ms = 1100; // define the full rest / pause

// Reference constants... for example, text speed...
// These constants contain values that are referenced internally to
// make external interfacing more intuitive
const TextSpeeds = new Array( // this array obsoletes userprefs.js
	2000,	// Ultra slow; 1 character/2 sec (or .5 character/sec)
	1000,	// Very slow; 1 character/sec
	500,	// Slow; 2 character/sec
	250,	// Normal; 4 character/sec
	100,	// Fast; 10 character/sec
	50,		// Very fast; 20 character/sec
	25,		// Ultra fast; 40 character/sec
	10,		// Extremely fast (for Kevin XD); 100 character/sec
	0			// Instant; infinite character/sec (almost)
);

const DefaultTextSpeed = 5; // index number of TextSpeeds array... which one is default?

function RelTextSpeed(integer) {
	// This function looks up a relative text speed, relative to what we think should
	// be the default... so integer=-2 is two steps below the default
	if(DefaultTextSpeed + integer < 0 || DefaultTextSpeed + integer >= TextSpeeds.length + 1) integer = 0;
	return TextSpeeds[DefaultTextSpeed + integer];
}


// This first line is the functdb.js entry... you'll see more like it around
try {notafunction();} catch(v) {TestRecord("FlashBox",new Array("style","text","title","vposition"),v.lineNumber+2,v.fileName,true);}
function FlashBox(style,text,title,vposition) {
	var x,y,w,h,cy;
	x = 1;
	w = ScreenWidth-2;
	h = TextFontHeight*BoxNumberOfLines;
	if(!vposition) vposition = "bottom";
	var cx = x;
	if(style) {
		switch(vposition.toLowerCase()) {
			case "bottom":
				y = ScreenHeight-h-1;
				cy = y-CaptFontHeight;
				break;
			case "middle":
				y = (ScreenHeight-h)/2;
				cy = y-CaptFontHeight;
				break;
			case "top":
				y = 1;
				cx = ScreenWidth-CaptFont.getStringWidth(title);
				cy = h+CaptFontHeight-7;
				break;
			case "fullscreen-primer":
				h = ScreenHeight-CaptFontHeight-1-2-10;
				y = CaptFontHeight-1+2+10;
				cy = y-CaptFontHeight;
				break;
			case "fullscreen":
				h = ScreenHeight-CaptFontHeight-1-2;
				y = CaptFontHeight-1+2;
				cy = y-CaptFontHeight;
				break;
			case "fullscreen-dev":
				h = ScreenHeight-(CaptFontHeight*3)-1-(2*3);
				y = CaptFontHeight-1+2+10;
				cy = y-CaptFontHeight;
				break;
		}
		//style.drawWindow(x,y,w,h);
	}
	/*if(title) CaptFont.drawText(cx,cy,title);
	TextFont.drawTextBox(x,y,w,h,Math.floor(TextFont.getHeight()/2),text);*/
	
	_gcy = cy;
	_gcx = cx;
	UniversalBox(x,y,w,h,style,text,title,true);
}

function InputBox(style,text,title) { // modified version of FlashBox()
	var x,y,w,h;
	x = 1;
	w = GetScreenWidth()-2;
	h = TextFont.getHeight()*4;
	y = (GetScreenHeight()-h)/2;
	/*var cx = x;
	var cy = y-CaptFont.getHeight();
	if(style) {style.drawWindow(x,y,w,h);}
	if(title) CaptFont.drawText(cx,cy,title);
	TextFont.drawTextBox(x,y,w,h,Math.floor(TextFont.getHeight()/2),text);*/
	UniversalBox(x,y,w,h,style,text,title,false);
}

var _gcy,_gcx; // these are for manual cx and cy overrides... mainly for optimization

try {notafunction();} catch(v) {TestRecord("TalkBox",new Array("style","text","title","vposition","speed"),v.lineNumber+2,v.fileName,true);}
function TalkBox(style,text,title,vposition,speed) { //try {notafunction();} catch(v) {funcline = v.lineNumber; funcfile = v.fileName;}
	//text = LineWrap(text); // Vexo library + extension added to fix line wrapping
	var ss = GetScreenBuffer();
	if(speed == undefined) {speed = 20;}
	var actual = "";
	var x,y,w,h,cy;
	x = 1;
	w = GetScreenWidth()-2;
	
	/* I believe that we need need two separate variables here: one for the checks on commands,
	 * and one for the actual string sent to the blitter. Now, how can we accomplish this, knowing that
	 * the command checker and blitter are horribly woven together?
	 */ // Or maybe not. (...?)
	text = LineBreakString(w,text); // Horray for DIY!
	
	h = TextFont.getHeight()*8;
	if(vposition == undefined) vposition = "bottom";
	var cx = x;
	switch(vposition.toLowerCase()) {
		case "bottom":
			y = GetScreenHeight()-h-1;
			cy = y-CaptFontHeight;
			break;
		case "middle":
			y = (GetScreenHeight()-h)/2;
			cy = y-CaptFontHeight;
			break;
		case "top":
			y = 1;
			cx = GetScreenWidth()-CaptFont.getStringWidth(title);
			cy = h+CaptFontHeight-7;
			break;
	}
	ClearKeyQueue();
	
	function __internal_wait(ms) {
		var ss = GetScreenBuffer();
		var start = GetTime();
		while(GetTime() < start + ms) {
			ss.blit(0,0);
			FlipScreen();
		}
	}
	
	_gcy = cy;
	_gcx = cx;
	
	//var stopfor = false;
	for(var i = 0; i <= text.length-1; i++) {
		ss.blit(0,0);
		
		// I hereby dedicate this if statement to Linda Kuhrau, who I asked to the prom on this date, Monday, March 27, 2006
		if(text[i] == "~") { // This if statement is here to try to detect any tilde commands that may be present.
			// NOTE: We need to shorten the string wrapper by three letters for each line for each command in said line (FIXED)
			// commands are (in dialogue text) formatted as follows: ~cn, where c is a command letter, and n is a number
			switch(text[i+1]) { // PLEASE KEEP IN MIND THIS IF STATEMENT IS COMPLETELY BOUND TO THE STRING WRAPPER
				case "p": // pause command
					// Note: PLEASE make sure that you don't do ~p0... that is an infinite wait
					UniversalBox(x,y,w,h,style,actual,title,true);
					__internal_wait(text_full_pause_ms/(parseInt(text[i+2]))); // wait for full pause divided by command number ms
					i += 2; // jump the cursor two places ahead to compensate for the tilde command, which will always be three letters
					break; // maybe fix with concat two substrings, removing current command
			}
		} else { // Heh, by the way, she said maybe, but after much complication, decided to go with Drew Blake.
			actual = actual+text[i]; // I'm now going with Kylee Underwood.
			//TextFont.drawTextBox(x+2,y,w-2,h,Math.floor(TextFont.getHeight()/2),actual);
			UniversalBox(x,y,w,h,style,actual,title,true);
			FlipScreen();
			if(wait(speed,true)) break;// stopfor = true;
			//break;
		}
		//if(stopfor) break;
	}
	
	ss.blit(0,0);
	/*if(style) style.drawWindow(x,y,w,h);
	if(title) CaptFont.drawText(cx,cy,title);*/
	//TextFont.drawTextBox(x+2,y,w-2,h,Math.floor(TextFont.getHeight()/2),text);
	UniversalBox(x,y,w,h,style,actual,title,true);
	GarbageCollect(); // clear function use memory
}

try {_notafunction();} catch(error) {postnote("The new Box constructors should be like... used and merged.",error);}

function Box(c_ul_rgb_a,c_ur_rgb_a,c_lr_rgb_a,c_ll_rgb_a) { // must be four arrays of RGB values
	this.c_ul_rgb_a = c_ul_rgb_a; // [0] is r
	this.c_ur_rgb_a = c_ur_rgb_a; // [1] is g
	this.c_lr_rgb_a = c_lr_rgb_a; // [2] is b
	this.c_ll_rgb_a = c_ll_rgb_a;
}

Box.prototype.drawWindow = function(x,y,w,h,alpha) {
	if(!alpha) alpha = 255;
	GradientRectangle(x,y,w,h, // border rectangle
		CreateColor(this.c_lr_rgb_a[0],this.c_lr_rgb_a[1],this.c_lr_rgb_a[2],alpha),
		CreateColor(this.c_ll_rgb_a[0],this.c_ll_rgb_a[1],this.c_ll_rgb_a[2],alpha),
		CreateColor(this.c_ul_rgb_a[0],this.c_ul_rgb_a[1],this.c_ul_rgb_a[2],alpha),
		CreateColor(this.c_ur_rgb_a[0],this.c_ur_rgb_a[1],this.c_ur_rgb_a[2],alpha));
	GradientRectangle(x+1,y+1,w-2,h-2, // main rectangle
		CreateColor(this.c_ul_rgb_a[0],this.c_ul_rgb_a[1],this.c_ul_rgb_a[2],alpha),
		CreateColor(this.c_ur_rgb_a[0],this.c_ur_rgb_a[1],this.c_ur_rgb_a[2],alpha),
		CreateColor(this.c_lr_rgb_a[0],this.c_lr_rgb_a[1],this.c_lr_rgb_a[2],alpha),
		CreateColor(this.c_ll_rgb_a[0],this.c_ll_rgb_a[1],this.c_ll_rgb_a[2],alpha));
	return true;
}

//var __testbox = new Box([20,200,50],[30,210,10],[50,180,30],[55,255,42]); // made by Ben and Chris
var __testbox = new Box([200,0,0],[200,0,0],[100,0,0],[100,0,0]);
//var __testbox = new Box([0,0,0],[31,31,31],[32,63,94],[31,31,31]);

try {notafunction();} catch(v) {TestRecord("UniversalBox",new Array("x","y","w","h","style","text","title","override"),v.lineNumber+2,v.fileName,true);}
function UniversalBox(x,y,w,h,style,text,title,override) {
	var cy = override? _gcy : y-CaptFontHeight;
	var cx = override? _gcx : x;
	if(style) __testbox.drawWindow(x,y,w,h);
	//if(style) __bloodbox(x,y,w,h);
	if(title) CaptFont.drawText(cx,cy,title);
	if(text) TextFont.drawTextBox(x+2,y,w-2,h,Math.floor(TextFont.getHeight()/2),text);
	//DrawZoomedTextBox(x+2,y,w-2,h,text);
}

/*try {notafunction();} catch(v) {TestRecord("univtest",new Array("testlevel"),v.lineNumber+2,v.fileName,false);}
function univtest(testlevel) {
	var x = 50;
	var y = x;
	var w = 300;
	var h = w;
	var style = Chara1;
	var text = "AsdFasddSaDSaDFaDASFDFFAaFDAFfADFaSFDFDAFDDFafdSDFSDFSAFDAFDSDFSFDfdasaddafafdafdafdfadAFDFDAAFDfdafadsfdasfdafdsafdafdafdafdafdaFDASA";
	var iterate = 1000;
	var cy = 75;
	var cx = 75;
	switch(testlevel) {
		case 2:
			for(var i = 0; i < iterate; i++) {
				UniversalBox(x,y,w,h,style,text,i);
				FlipScreen();
			}
			break;
		case 3:
			for(var i = 0; i < iterate; i++) {
				OptimBox(x,y,w,h,style,text,i);
				FlipScreen();
			}
			break;
		case 4:
			_gcy = cy;
			_gcx = cx;
			for(var i = 0; i < iterate; i++) {
				OptimBox(x,y,w,h,style,text,i,true);
				FlipScreen();
			}
			break;
	}
	ClearKeyQueue();
}*/

// ErrorBox() stuff merged from starter
// NOTE: Do _NOT_ move ErrorBox() over to the UniversalBox() core. If an error happens, we want
// to minimize as many function calls as possible.
const whichstyledoesthisthinguse = Chara; //This defines which windowstyle the loader will use
var errorimg = LoadImage("ui/cerror.png");
function ErrorBox(text,title) { // merged from starter
	// This is a heavily hacked version of BM 0.1.3's window.js FlashBox().
	// It also sports a very sexy LGPL error image now. That's hot.
	style = whichstyledoesthisthinguse;
	var x,y,w,h;
	x = 1;
	w = GetScreenWidth()-2;
	h = TextFont.getHeight()*8;
	var cx = x;
	// Most of the styleization code from here has been removed and replaced with defaults.
	// If you're looking for the checks and switches, they're gone. The would have been
	// here, where this comment now lies.
	y = (GetScreenHeight()-h)/2;
	style.drawWindow(x,y,w,h);
	//This seems to be a number I'm using a lot... magic is apparently the magic number XD
	var magic = (h-errorimg.height)/2;
	//This next number will be used for the text... it's lucky that I figured it out XD
	var lucky = magic*2+errorimg.width-1;
	errorimg.blit(1+magic,y+magic);
	var cy = y-CaptFont.getHeight();
	if(title) CaptFont.drawText(cx,cy,title);
	TextFont.drawTextBox(x+lucky,y,w-lucky,h,Math.floor(TextFont.getHeight()/2),text);
}
try {notafunction();} catch(k) {TotalLineCount += k.lineNumber+1; FileLineCount[k.fileName.replace(/scripts\//i,"")] = k.lineNumber+1;}