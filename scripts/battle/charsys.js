/* BMPROJECT FILE: battle/charsys.js - Character Systems

SYNOPSIS:
	This file controls how characters are maintained by managing hit points
	and other stats, levels and experience, battle conditions, and other
	stats.

CONTENT:
	Contains the character frameworks.

LICENSE(S):
	GNU General Public License (see zeroinit.js)

*/

function Character() { // the Character object
	this.current = new Object(); // this contains the current values
	this.maximum = new Object(); // ... as opposed to maximum values
	// Hrm, creepy. Those lines are the exact same length XD
	this.current.hp = 0;
	this.maximum.hp = 0;
	this.current.ap = 0;
	this.maximum.ap = 0;
	
	this.experience = 0;
	this.current.level = 0;
	
}

// This needs finished