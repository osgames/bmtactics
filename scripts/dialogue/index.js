// The Betrayer's Moon Dialogue Library
// For now, this will be only for cutscenes/special events.
// There might be another way to do casual conversation, as
// it doesn't require being so choreographed.

// All of the Chapters should go here.
MakeNextChapter("The Beginning");
// MakeNextChapter("Next Chapter");

// All of the Scenes should go here.
// Chapter 1
MakeNextScene(1,"Introduction");

// Now find and compile all of them, make-style
// Directory structure should be organized like so:
// dialogue/chapter3/scene2.js
//
/* scene2.js should have this form:
pw().dialogue[0] = "(Enter Left Ripley [normal])";
pw().dialogue[1] = "Ripley (normal): I hope Taylor gets here soon...";
pw().dialogue[2] = "(Enter Right Taylor [normal])";
*/

for(var i = 1; i <= chapterindex.length; i++) {
	currentchapter = i;
	for(var j = 1; j <= GetChapter(i).sceneIndex.length; j++) {
		currentscene = j;
		EvaluateScript("dialogue/chapter" + i + "/scene" + j + ".js");
		GetChapter(i).GetScene(j).compile();
	}
}