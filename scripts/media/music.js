// Betrayer's Moon Music Database -- dremelofdeath 2k5
function MusicSlot(name,extension) {
	this.name = name;
	this.sound = undefined;
	this.shouldsound = name+"."+extension;
	this.isLoaded = false;
	this.isStreaming = false;
	this.OverrideAbort = true;
}
MusicSlot.prototype.LoadMusic = function() {
	if(!this.isLoaded) {
		this.sound = LoadSound(this.shouldsound,this.isStreaming);
		this.isLoaded = true;
	} else {
		if(!this.OverrideAbort) Abort("Tried to load a song over a previously loaded slot.");
	}
}
MusicSlot.prototype.ForceLoadMusic = function(what) {
	if(!this.isLoaded) {
		this.sound = LoadSound(what,this.isStreaming);
		this.isLoaded = true;
	} else {
		if(!this.OverrideAbort) Abort("Tried to forceload a song over a previously loaded slot.");
	}
}
MusicSlot.prototype.UnloadMusic = function() {
	if(this.isLoaded) {
		this.sound = undefined;
		this.isLoaded = false;
	}
}
//Music Library
function MusicLibraryService(selfname) {
	this.name = selfname;
	this.UniqueID = new Array();
	this.magicaltest = new MusicSlot("Test","mp3");
	this.UniqueID.push("magicaltest");
	this.intro = new MusicSlot("Introduction","mp3");
	this.UniqueID.push("intro");
	this.battle = new MusicSlot("Battle","mp3");
	this.UniqueID.push("battle");
}
MusicLibraryService.prototype.GetSlotByID = function(id) {
	eval("var asdf = "+this.name+"."+this.UniqueID[id]+";");
	return asdf;
}
var MusicLibrary = new MusicLibraryService("MusicLibrary");
try {notafunction();} catch(k) {TotalLineCount += k.lineNumber+1; FileLineCount[k.fileName.replace(/scripts\//i,"")] = k.lineNumber+1;}