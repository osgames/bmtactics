// Betrayer's Moon Effects, screen transition whatnot DREMELOFDEATH2K5
function TransitionService() {
	this.randomwhatever = "Sure, why not?";
}
TransitionService.prototype.BuggedOne = function(what,speed,time) {
	var w = what.width;
	var h = what.height;
	for(var i = 1; i <= time; i += speed) {
		what.transformBlit(0-i,0-i,w+i,h-i,w+i,0+i,0-i,h+i);
		FlipScreen();
	}
}
TransitionService.prototype.PlanarIn = function(what,speed,time) {
	var w = GetScreenWidth();
	var h = GetScreenHeight();
	for(var i = time; i >= 0; i -= speed) {
		what.transformBlit(0+i,0+i,w+i,0+i,w+i,h+i,0-i,h+i);
		FlipScreen();
	}
}
TransitionService.prototype.PlanarOut = function(what,speed,time) {
	var w = GetScreenWidth();
	var h = GetScreenHeight();
	for(var i = 1; i <= time; i += speed) {
		what.transformBlit(0-i,0-i,w+i,0-i,w-i,h-i,0-i,h-i);
		FlipScreen();
	}
}
TransitionService.prototype.FadeIn = function(what,speed,r,g,b,skip) {
	ClearKeyQueue();
	var skipit = false;
	if(skip == undefined) skip = false;
	var ss = CreateSurface(GetScreenWidth(),GetScreenHeight(),CreateColor(r,g,b));
	ss.setAlpha(0);
	for(var i = 255; i >= 0; i -= speed) {
		if(skip && AreKeysLeft()) if(GetKey() == KEY_ENTER) {
			skipit = true;
			break;
		}
		what.blit(0,0);
		ss.setAlpha(i);
		ss.blit(0,0);
		//Global.freespace = GetScreenBuffer();
		// global.js was removed... if possible, create a not-so-gay method of accomplishing
		// whatever it is you wanted to accomplish
		FlipScreen();
	}
	return skipit;
}
TransitionService.prototype.FadeOut = function(what,speed,r,g,b,skip) {
	ClearKeyQueue();
	if(skip == undefined) skip = false;
	var ss = CreateSurface(GetScreenWidth(),GetScreenHeight(),CreateColor(r,g,b));
	ss.setAlpha(0);
	var sys = undefined;
	for(var i = 1; i <= 255; i += speed) {
		if(skip && AreKeysLeft()) if(GetKey() == KEY_ENTER) break;
		what.blit(0,0);
		ss.setAlpha(i);
		ss.blit(0,0);
		sys = GetScreenBuffer();
		FlipScreen();
	}
	return sys;
}
TransitionService.prototype.FadeOutHalt = function(what,speed,r,g,b,a/*,skip*/) {
	var ss = CreateSurface(GetScreenWidth(),GetScreenHeight(),CreateColor(r,g,b));
	ss.setAlpha(0);
	var sys = undefined;
	for(var i = 1; i <= a; i += speed) {
		//if(skip && AreKeysLeft()) if(GetKey() == KEY_ENTER) break;
		what.blit(0,0);
		ss.setAlpha(i);
		ss.blit(0,0);
		sys = GetScreenBuffer();
		FlipScreen();
	}
	return sys;
}
TransitionService.prototype.FadeInFrom = function(what,speed,r,g,b,a/*,skip*/) {
	var ss = CreateSurface(GetScreenWidth(),GetScreenHeight(),CreateColor(r,g,b));
	ss.setAlpha(a);
	for(var i = a; i >= 0; i -= speed) {
		//if(skip && AreKeysLeft()) if(GetKey() == KEY_ENTER) break;
		what.blit(0,0);
		ss.setAlpha(i);
		ss.blit(0,0);
		FlipScreen();
	}
}
TransitionService.prototype.ZoomIn = function(what,speed,time) {
	var w = GetScreenWidth();
	var h = GetScreenHeight();
	for(var i = 1; i <= time; i += speed) {
		what.transformBlit(0-i,0-i,w+i,0-i,w+i,h+i,0-i,h+i);
		FlipScreen();
	}
}
TransitionService.prototype.ZoomInExponent = function(what,speed,time) {
	var w = GetScreenWidth();
	var h = GetScreenHeight();
	for(var i = 1; i <= time; i += speed) {
		what.transformBlit(0-(i*i),0-(i*i),w+(i*i),0-(i*i),w+(i*i),h+(i*i),0-(i*i),h+(i*i));
		FlipScreen();
	}
}
TransitionService.prototype.ZoomOut = function(what,speed,time) {
	var w = GetScreenWidth();
	var h = GetScreenHeight();
	for(var i = time; i >= 0; i -= speed) {
		what.transformBlit(0-i,0-i,w+i,0-i,w+i,h+i,0-i,h+i);
		FlipScreen();
	}
}
TransitionService.prototype.ZoomOutExponent = function(what,speed,time) {
	var w = GetScreenWidth();
	var h = GetScreenHeight();
	for(var i = time; i >= 0; i -= speed) {
		what.transformBlit(0-(i*i),0-(i*i),w+(i*i),0-(i*i),w+(i*i),h+(i*i),0-(i*i),h+(i*i));
		FlipScreen();
	}
}
TransitionService.prototype.Melt = function(what,columnWidth,time,speed,color,type) {
	what.blit(0,0);
	tMelt(columnWidth,time,speed,color,type);
}
TransitionService.prototype.DefaultMelt = function(what) {
	what.blit(0,0);
	tMelt(2,7,5,CreateColor(0,0,0),1);
}
TransitionService.prototype.Swish = function(what,speed,time,color) {
	what.blit(0,0);
	tSwish(speed,time,color);
}
TransitionService.prototype.DefaultSwish = function(what) {
	what.blit(0,0);
	tSwish(2,4000,CreateColor(0,0,0));
}
var Transition = new TransitionService();
try {notafunction();} catch(k) {TotalLineCount += k.lineNumber+1; FileLineCount[k.fileName.replace(/scripts\//i,"")] = k.lineNumber+1;}