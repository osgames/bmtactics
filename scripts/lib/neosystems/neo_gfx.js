// GRAPHICS SYSTEM v1.0 + SCREEN FUNCTIONS (rev. NeoLogiX v1.1)
// some graphics functions & revised screen functions
// (includes functions from the graphics system script b/c they're small)
// more functions will be added as i think of them
// (will NOT add the circle functions from the circles system script as
//  geno023 worked hard to get his working, so if you want circles, import
//  the circle script physically or use EvaluateSystemScript("circles.js"); )
// possibly some text and/or windowstyle functions may be added,
//
// send any suggestions to LiquidMeTaL2kX@aol.com or post a request TO ME
// on sphere forums (http://sphere.sourceforge.net/forums/)
// - N e o L o g i X   (v1.0 c2004/01.31)
// - N e o L o g i X   (v1.1 c2004/??.??)

// note: i would rewrite TraceRectangle/Poly to use TraceGradientRectangle/Poly
// but in this case i'm a memory cheapskate (yes, i know they're LINE functions)
// so i let the original functions be
//
// TraceRectangle() & TracePoly() by rizen

RequireSystemScript("colors.js");
// GRAPHICS FUNCTIONS
function TraceRectangle(x, y, w, h, c) {
  Line(x, y, x+w, y, c);
  Line(x, y, x, y+h, c);
  Line(x+w, y, x+w, y+h, c);
  Line(x, y+h, x+w, y+h, c);
}

function TracePoly(x, y, c) {
  for (i = 1;i<x.length;i++) {
    Line(x[i-1],y[i-1],x[i],y[i], c);
  }
  Line(x[x.length-1],y[y.length-1],x[0],y[0],c);
}

function TraceGradientPoly(x,y,c) {
 // should draw a poly w/gradient lines (hope this works...)
 // ideally, w/this & TracePoly() x,y,c have same lengths
 for (var i=1; i<x.length; i++) {
  var yI = i%y.length; // fix just in case
  var cI = i%c.length; // fix just in case
  GradientLine(x[i-1],y[yI-1], x[i],y[yI], c[cI-1],c[cI]);
  }
 GradientLine(x[x.length-1],y[y.length-1], x[0],y[0], c[c.length-1],c[0]);
 }

function TraceGradientRectangle(x,y,w,h, c_ul,c_ur,c_lr,c_ll) {
 // takes 4 colors (upper left, then right, then lower right, then left)
 GradientLine(x,y, x+w,y, c_ul,c_ur);
 GradientLine(x+w,y, x+w,y+h, c_ur,c_lr);
 GradientLine(x+w,y+h, x,y+h, c_lr,c_ll);
 GradientLine(x,y+h, x,y, c_ll,c_ul);
 }

function TraceGradientTriangle(x1,y1,x2,y2,x3,y3, c1,c2,c3) {
 // c1 is x1,y1, c2 is x2,y2, c3 is x3,y3
 GradientLine(x1,y1, x2,y2, c1,c2);
 GradientLine(x2,y2, x3,y3, c2,c3);
 GradientLine(x3,y3, x1,y1, c3,c1);
 }

// SCREEN FUNCTIONS
// use EvaluateScript("neo_gfx.js") or RequireScript("neo_gfx.js")
// & these functions should replace the functions defined in screen.js
function ClearScreen() {
 ApplyColorMask(Black);
 }

function FadeOut(ms) {
 FadeToColor(ms, Black);
 }

function FadeIn(ms) {
 FadeFromColor(ms, Black);
 }

function FadeToColor(msecs, clr) {
 var w = GetScreenWidth(), h = GetScreenHeight();
 var img = GrabImage(0, 0, w, h);
 var color = CreateColor(clr.red, clr.green, clr.blue, clr.alpha);
 var time = GetTime();
 var time2 = GetTime();
 var step = 255/msecs;
 while (time2-time<msecs) {
  color.alpha = (time2-time)*step;
  img.blit(0, 0);
  ApplyColorMask(color);
  //Rectangle(0,0, w,h, color);
  FlipScreen();
  time2 = GetTime();
  }
 color.alpha = 255;
 img.blit(0, 0);
 ApplyColorMask(color);
 FlipScreen();
 img.blit(0, 0);
 }

function FadeFromColor(msecs, clr) {
 var w = GetScreenWidth(), h = GetScreenHeight();
 var img = GrabImage(0, 0, w, h);
 var color = CreateColor(clr.red, clr.green, clr.blue, clr.alpha);
 ApplyColorMask(color);
 FlipScreen();
 var time = GetTime();
 var time2 = GetTime();
 var step = 255/msecs;
 while (time2-time<msecs) {
  color.alpha = 255-(time2-time)*step;
  img.blit(0, 0);
  ApplyColorMask(color);
  //Rectangle(0,0, w,h, color);
  FlipScreen();
  time2 = GetTime();
  }
 color.alpha = 0;
 img.blit(0, 0);
 ApplyColorMask(color);
 FlipScreen();
 img.blit(0, 0);
 }
