/* FIXME Debug Information, by dremelofdeath.
 * Copyright 2006. Released under the GNU General Public License v2.
 *
 * fixme.js adds a fixme() function that allows a programmer to leave a
 * note to himself. When the fixme() call is reached, the engine will
 * drop everything and display a little reminder about the fixme. After
 * the programmer presses a key, the engine returns to what it was doing
 * and continues execution.
 *
 * I hope you find this as useful as I have.
 */

/* INSTRUCTIONS
 * If you prefer to not use the bmproject fixme style, which will only
 * use the bmproject libraries if they are available, then you have two
 * options. First, you could make your own fixme style and add it to the
 * list, or you could simply change the __use_fixme_style to general.
 * "general" is guaranteed to work with any Sphere project, so you won't
 * have any problems with libraries or fonts.
 */

/* LIST OF STYLES AVAILABLE
 * "general" - Should work with any Sphere project.
 * "bmproject" - Should work only with the most recent Betrayer's Moon Project release.
 */ // By the way, feel free to submit your own project's fixme style to this. It'll make things interesting.

const __use_fixme_style = "bmproject";

/* The fixme() function
 * message - A string to display as a reminder to yourself
 * filename (optional) - The filename where the fixme resides
 * linenumber (optional) - The linenumber of the fixme
 *
 * The fixme() function is basically just a frontend/shortcut to the others. If you find
 * yourself using specifically one type of fixme all of the time, change the constant
 * and use fixme() instead. It makes things quick and easy when programming.
 *
 * Also, please keep in mind these are the _suggested_ uses of variables. They are meant
 * to be a template only. Please see the individual function to see how it handles
 * variables, as fixme() only passes the arguments.
 *
 * To achieve the filename and linenumber functionality, you can type them yourself, or
 * you can try a try/catch block to get it. (I do this frequently in Betrayer's Moon.)
 * For example:
 * try {notafunction();} catch(error) {fixme_general("Remember to fix this.",error.fileName,error.lineNumber+1);}
 *
 * See? You do learn something new everyday.
 */

function fixme(message,filename,linenumber) {
	eval("fixme_"+__use_fixme_style+"(message,filename,linenumber);");
}

function fixme_general(message,filename,linenumber) { // this should not be dependent on any libraries or functions
	CreateSurface(GetScreenWidth(),GetScreenHeight(),CreateColor(0,0,0)).blit(0,0);
	var sysfont = GetSystemFont();
	var delta = sysfont.getHeight();
	if(linenumber && filename) {
		sysfont.drawText(delta,delta*1,"FIXME in file " + filename + ", line " + linenumber + ":");
	} else if(filename && !linenumber) {
		sysfont.drawText(delta,delta*1,"FIXME in file " + filename + ":");
	} else {
		sysfont.drawText(delta,delta*1,"FIXME message:");
	}
	sysfont.drawText(delta,delta*2,message);
	while(AreKeysLeft()) {void GetKey();}
	FlipScreen();
	void GetKey();
}

// Since fixme_bmproject() will always be called from within a try/catch block, we'll
// just pass the error object thrown.
// Also, the bmproject's fixme is designed to match the rest of the program... I guess
// it's a style thing XD
// try {notafunction();} catch(error) {fixme("Remember to fix this.",error);}
function fixme_bmproject(message,error_object) {
	var fonttouse = TextFont? TextFont : GetSystemFont();
	var titlefontuse = TitleFont? TitleFont : GetSystemFont();
	var texttodisplay = GameTitle;
	var btextx,btexty;
	if(typeof FlushScreenBuffer == "function") {
		FlushScreenBuffer();
	} else {
		FlipScreen();
		FlipScreen();
	}
	if(typeof ErrorBox == "function") {
		fonttouse.drawTextBox(0,GetSystemFont().getHeight(),GetScreenWidth(),GetScreenHeight(),0,error_object.stack);
		btextx = GetScreenWidth() - titlefontuse.getStringWidth(texttodisplay);
		btexty = GetScreenHeight() - titlefontuse.getHeight();
		titlefontuse.drawText(btextx,btexty,texttodisplay); // <-- logo here
		var ss = GetScreenBuffer();
		CreateSurface(GetScreenWidth(),GetScreenHeight(),CreateColor(0,0,0)).blit(0,0);
		ss.blitMask(0,0,CreateColor(100,100,100));
		ErrorBox(message,"FIXME: " + error_object.fileName.toUpperCase() + ", LINE " + (error_object.lineNumber+1));
	} else {
		fonttouse.drawTextBox(0,20,GetScreenWidth(),GetScreenHeight()-20,0,"FIXME: "+error_object.fileName.toUpperCase()+", LINE "+(error_object.lineNumber+1)+"\n\n"+message);
	}
	FlipScreen();
	while(AreKeysLeft()) {void GetKey();}
	void GetKey();
}