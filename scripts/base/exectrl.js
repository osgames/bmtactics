/* BMPROJECT FILE: exectrl.js - Program Execution Control and Headers

SYNOPSIS:
	This file controls how the program executes and exits.

EXECUTION:
	This script replaces all of the information in the zeroinit.js headers
	before game(), thus cleaning up all of the scattered information there.

CONTENT:
	Contains the zeroinit.js header information, includes, global variables
	and constants declared in zeroinit.js, and some functions related to
	starting and exiting the program.

NOTES:
	This is the final area for zeroinit.js header information. New or
	testing headers should first be written in zeroinit.js, and, upon
	completion and acceptance into the official tree, should be merged into
	exectrl.js.

LICENSE(S):
	GNU General Public License (see zeroinit.js)

*/

//////////      HEADERS      //////////

// I think this has highest priority; let's check the startup time... start the timer.
EvaluateScript("base/chrono.js");
StartDebugTimer();

// I do believe this subproject should be permanently abandoned. It doesn't really make sense to have its
// own launcher. However, maybe I'll put a little effort into writing my own startup for Betrayer's Moon.
// That, if I decide to do it, will show up in 0.2.1.

// GLOBAL.JS IS DEAD! THIS HISTORIC MOMENT: 01/29/06 10:45 PM EST

var ShowDeveloperMenu = true; // changed to var to make it internally accessible (eg internal debug reset)
const StepThroughLoad = false;
const ForcePreloaderSlow = false;
const PreloaderSlowdown = 750;
const ShowPreloader = true; // Change to false to get a black screen during load

/* The Goal of BM:

(00:55:29) Zachary Murray: Will the ending be surreal enough to stun an elephant?

Good Firefox Hunting

*/

EvaluateScript("base/general.js");
EvaluateScript("ioctrl/errorh.js");
try { // okay, I think I'm happy now XD


// We're going to load the general (or global) preferences file here.
// That way, we can close it right before the functions so we can have
// access to it during the loading process. I think it's a good idea.
var PreferencesFile = OpenFile("default.cfg");


// Stop the timer and record. Restart the timer for the Includes.
const PreStartupTime = GetDebugTimeElapsed();
ResetDebugTimer();

EvaluateScript("base/include.js");

// Scripts that are evaluated BEFORE Includes:
// base/general.js, ioctrl/errorh.js, base/include.js

IncludeSystem("circles.js");
// Begin moving all code that is not written by our team (i.e. me) to the IncludeScriptOtherLicense, whether or not it's free
IncludeScriptOtherLicense("ioctrl/blocktea.js","Used with permission. Copyright 2002-2005 Chris Veness, all rights reserved. http://www.movable-type.co.uk");
IncludeScriptOtherLicense("ioctrl/movement.js","Used with permission. Copyright Brian Robb (flik). Most probably under GPL.");
IncludePriority("base/fixme.js");
IncludePriority("base/postnote.js");
IncludePriority("base/version.js");
IncludePriority("base/functdb.js"); // The Function Test Database functions... these let the functions in the program be tested by devmenu
IncludePriority("videoui/vbuffer.js");
IncludePriority("videoui/window.js");
// NeoSystems libraries... by NeoLogiX
/*IncludeScript("lib/neosystems/compat.js");
IncludeScript("lib/neosystems/f_sys.js");
IncludeScript("lib/neosystems/f_rm.js");					// These libraries need reevaluated to see if they
IncludeScript("lib/neosystems/f_check.js");			// are actually needed or not, as I'm not sure.
*/IncludeScript("lib/neosystems/f_trans.js");
/*IncludeScript("lib/neosystems/neo_ringmenu.js");
IncludeScript("lib/neosystems/neo_gfx.js");
IncludeScript("lib/neosystems/neo_window.js"); // Are these used at all?
*/
// Vexo's libraries
//IncludeScript("lib/vexo/stringw.js");
/* base - Basic, low-level scripts.
	These scripts are the lowest level scripts. They do something internally, like
	extend a library or add functions that are so general that they replace often used
	procedures.
	
	What belongs here:
		A general functions script.
		A library extension script.
	What doesn't belong here:
		A screen transition script.
		A keyboard input script.
*/
//IncludeScript("base/general.js"); // Other than not being documented, the functions are good ideas. This needs trashed and the functions put into more intelligent places
/* ioctrl - I/O Control Scripts. 
	The ioctrl scripts are used to control input or output (including error output).
	Examples of I/O controls scripts would be a keyboard script or a menu script. ioctrl
	can (and probably should) deal with user interfaces.
	
	What belongs here:
		A keyboard input script.
		A mouse input script.
	What doesn't belong here:
		Sound-related scripts.
*/
IncludeScript("ioctrl/input.js");
IncludeScript("ioctrl/usrprefs.js");
IncludeScript("ioctrl/loadman.js"); // This needs rewritten, although it's a good idea. It's a style issue
// Another note: does loadman.js really belong in ioctrl?
// Another another note: I don't think it should; and loadman isn't really a good name.
// I think loadscrn is much better and more descriptive. Plus, on looking at the code,
// it needs a complete rewrite.
IncludeScript("ioctrl/dialogue.js"); // The portrait system needs a rewrite, perhaps for 0.2.1?
/* menu / videoui - Menu Scripts and Menus.
	This is self-explanatory... these are either menu systems or menus that are loaded
	into memory on startup. Pretty simple concept, if you ask me.
*/
IncludeScript("videoui/menusys.js");
IncludeScript("videoui/devmenu.js"); // The DeveloperMenu() and related functions. zeroinit was getting too big
IncludeScript("videoui/render.js");
IncludeScript("videoui/update.js");
IncludeScript("videoui/scrsys.js");
/* media - Media-related Scripts.
	The media scripts are used to play or manipulate sound or video (or both).
	
	What belongs here:
		A script that loads music files.
		A script that keeps track of currently playing music.
		A script of screen transitions.
	What doesn't belong here:
		A loading screen script.
*/
IncludeScript("media/music.js"); // This might need redone, and I don't really like the code. It looks homosexual
IncludeScript("media/jukebox.js"); // It might be ugly, but it works.
IncludeScript("media/soundfx.js");
IncludeScript("media/effect.js"); // Screen transitions and the like... it works.
// BM Dialogue
// This one include will begin compiling,  at least for
// now,  the entire library of dialogue for the whole
// game.
IncludeScript("dialogue/index.js"); // surprisingly, this is one thing I don't hate XD
// However, it does need removed so it can be merged with the preloader.
// index.js has been removed so it can be loaded concurrently with the preloader
// or at least, will be removed

IncludeScript("battle/thinkai.js");

CallScriptLoader();

// Reset the timer again
const IncludeTime = GetDebugTimeElapsed();
ResetDebugTimer();

// Close the preferences file here... flush any changes
PreferencesFile.flush();

// Let's check how much time it took to start up.
const PostStartupTime = GetDebugTimeElapsed();
StopDebugTimer();
const StartupTime = PreStartupTime + IncludeTime + PostStartupTime;

//////////      EXECTRL.JS FUNCTIONS      //////////

function GameExit() {
	SetFrameRate(0);
	Transition.DefaultMelt(GetScreenBuffer());
	Exit();
}

//////////      END OF FILE      //////////

} catch(error) {
	HandleException(error);
	set_die_flag(); // ugly hack to fix Exit() crash before code execution (spheredev.net forums ID #1056)
} // End error handler protection
try {notafunction();} catch(k) {TotalLineCount += k.lineNumber+1; FileLineCount[k.fileName.replace(/scripts\//i,"")] = k.lineNumber+1;}