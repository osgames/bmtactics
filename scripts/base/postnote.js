/* Postnote system, by dremelofdeath.
 * Copyright 2006. Released under the GNU General Public License v2.
 *
 * All postnote() does is leave a message in the global scope. They are all
 * displayed upon calling show_postnotes(). Pretty useful for to do lists,
 * fixmes, and the like.
 *
 * I hope you find this as useful as I have.
 */

// example code to leave a postnote
// try {_notafunction();} catch(error) {postnote("Hey, fix that.",error);}

var __postnotedb = new Array();
var __postnotefont = GetSystemFont();
var __postnotecustomdisp = false;

function setpostnotefont(font) {
	if(font) __postnotefont = font.clone();
}

function setpostnotecustomdisp(custdisp) { // set a custom display for show_postnotes()
	if(custdisp !== undefined) __postnotecustomdisp = custdisp;
}

function postnote_object(message,error_object) {
	this.message = message;
	this.error = error_object;
}

function postnote(message,error_object) {
	__postnotedb.push(new postnote_object(message,error_object));
}

function show_postnotes() {
	FlipScreen(); // clear whatever's on the video backbuffer
	FlipScreen();
	var delta = __postnotefont.getHeight();
	var showtext = __postnotecustomdisp? __postnotecustomdisp : "Current postnotes:";
	__postnotefont.drawText(10,10,showtext);
	if(__postnotedb.length != 0) {
		for(var i = 0; i < __postnotedb.length; i++) {
			__postnotefont.drawText(10,delta*(i+0.75)*2+10,__postnotedb[i].error.fileName+", line "+(__postnotedb[i].error.lineNumber+1)+":");
			__postnotefont.drawText(25,delta*(i+1.25)*2+10,__postnotedb[i].message);
		}
	} else {
		__postnotefont.drawText(10,delta*1.5+10,"No current postnotes.");
	}
	while(AreKeysLeft()) {void GetKey();}
	FlipScreen();
	GetKey();
}