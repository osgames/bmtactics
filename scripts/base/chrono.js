/* BMPROJECT FILE: base/chrono.js - Timing functions for debugging purposes

SYNOPSIS:
	This script allows functions to be "timed" so that they can be easily
	optimized. This "timing" returns the amount of time between the
	beginning and end of the function.

EXECUTION:
	None in itself - anything that wishes to use the chrono library must
	make the calls to start the timer and to get the timer. No function
	should execute with the chrono library itself unless that function
	specifically tests other functions.

CONTENT:
	The chronorepository variable, which is a global placeholder for the
	amount of time that is being counted for the function execution. Also
	contains the functions that run the chrono library.

LICENSE(S):
	GNU General Public License (see zeroinit.js)

*/
/* Betrayer's Moon Project Chrono Library (Timing Functions)
Hopefully, this library can be reused. I expect it to be useful for finding
just how fast functions can work. This could be used to benchmark things, I
suppose.

It shouldn't be dependent on any BM scripts, so take away; it's GPL'd.
*/

var chronorepository = 0;

function TimerError() {
  this.name = "TimerError";
  this.message = "Timer was not started or has been stopped";
  this.description = this.message;
}
TimerError.prototype = new Error;

function StartDebugTimer() {chronorepository = GetTime();}

function StopDebugTimer() {chronorepository = 0;}

function ResetDebugTimer() {StartDebugTimer();} // synonym

function GetDebugTimeElapsed() {
	var currenttime = GetTime();
	if(chronorepository == 0) {
		throw new TimerError();
		//Abort("Timer was not started or has been stopped");
	} else {
		return currenttime - chronorepository;
	}
}

function EndDebugTiming() {
	var chrono = GetDebugTimeElapsed();
	StopDebugTimer();
	return chrono;
}
try {notafunction();} catch(k) {TotalLineCount += k.lineNumber+1; FileLineCount[k.fileName.replace(/scripts\//i,"")] = k.lineNumber+1;}