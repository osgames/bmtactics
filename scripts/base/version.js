/* BMPROJECT FILE: base/version.js - Internal Version Information

SYNOPSIS:
	This file defines the the version and version titles, along with some
	other information about the current version.

EXECUTION:
	Defines several consts that are used globally. The game will
	automatically reflect any changes made here.

CONTENT:
	The game title, version title, version number, last version, release
	date of current version, known bugs, and changes from last version.

LICENSE(S):
	GNU General Public License (see zeroinit.js)

*/
/* Version information and build options
	As a side note, these should probably not be changed to var, which
	would make them user-accessible. If, however, you would like to make
	one of them internally or externally accessible, change const to var,
	which will make it possible to be changed. You had better have a good
	reason for doing this, however, as building on top of a var that
	should be a const can be dangerous if you change it back to const.
*/

const GameTitle = "Betrayer's Moon";
const VersionTitle = "Dale Memorial Release";
const CurrentVersion = new Array(0,2,5);
const CurrentVersionString = "" + CurrentVersion[0] + "." + CurrentVersion[1] + "." + CurrentVersion[2];
const UpgradeFrom = new Array(0,2,4);
const UpgradeFromString = "" + UpgradeFrom[0] + "." + UpgradeFrom[1] + "." + UpgradeFrom[2];
const ReleaseDate = "April 1, 2006";

const KnownBugs = new Array();
const Changes = new Array();

Changes.push("");